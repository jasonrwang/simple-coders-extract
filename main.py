# %% [markdown]
# # Simple CODERS Data Extract
#
# This is the purpose of this project.....

# %%
import pandas as pd
import altair as alt

# %%
interestingVar = 'generators'
IP = FILL_THIS_IN

# %%
# Read in the JSON data
df = pd.read_json(f'http://{IP}/{interestingVar}')

# %%
df.to_excel('Generators.xlsx')

# %%
alt.Chart(df).mark_bar().encode(
    x='owner:O',
    y='sum(install_capacity_in_mw):Q',
    color='gen_type:N',
    facet='province:N'
)

# %%
